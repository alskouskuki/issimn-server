﻿const connection = new signalR.HubConnectionBuilder()
    .withUrl("http://localhost:5000/openHub")
    .configureLogging(signalR.LogLevel.Information)
    .build();

$(document).ready(function () {
    
    if (window.localStorage.getItem("username") === null || window.localStorage.getItem("username") === "") {
        window.location = "http://localhost:5002/Home/Login";
    }
    else {
        $('#userName-1').text("Вы вошли как: " + window.localStorage.getItem("username"));
        connection.start().then(function () {
            var name = window.localStorage.getItem("username");
            connection.invoke("BackConnectedAsync", name);
        });
    }
})


connection.on("LoginStatus", (response) => {
    if (response === 401) {
        window.location = "http://localhost:5002/Home/Login";
        window.localStorage.removeItem("username");
        alert("Username is not correct!");
    }
});

connection.on("RestoreMessages", (response) => {
    if (response === undefined) {
        console.log("not have");
    }
    else {
        console.log(response);
    }
});

connection.on("RestoreSubscriptions", (response) => {
    if (response === "[]") {
    } else {
        console.log(response);
    }
});
connection.on("LoginStatus", (response) => {
    console.log(response);
    if (response === 200) {
        $('<li><a href="home/control">Перейти в режим в администратора</a></li>').appendTo('#dropcontextcontrolmenu');
    }

    if (response === 207) {
        $('<li><a href="home/control">Комната создателя</a></li>').appendTo('#dropcontextcontrolmenu');
    }
});