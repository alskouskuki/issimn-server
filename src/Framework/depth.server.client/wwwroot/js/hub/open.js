﻿const connection = null;

$(document).ready(function () {
    buildOpenConnection();
    connection.start().then(function () {
        connection.invoke("BackConnectedAsync", window.localStorage.getItem("username"));
    });
});

connection.on("Error", (response) => {
    if (response === null) {
        window.location = "http://192.168.1.3:5002/Home/Login";
    }
});

function buildOpenConnection() {
    connection = new signalR.HubConnectionBuilder()
        .withUrl("http://192.168.1.3:5000/openHub")
        .configureLogging(signalR.LogLevel.Information)
        .build();
}