﻿const admConnection = new signalR.HubConnectionBuilder()
    .withUrl("http://localhost:5000/adminHub")
    .configureLogging(signalR.LogLevel.Information)
    .build();
$(document).ready(function () {
    if (admConnection === undefined) {
        alert("error!");
    } else {
        admConnection.start().then(function () {
            $('#userName-1').text("С возвращением,  " + window.localStorage.getItem("username") + "!");
            var name = window.localStorage.getItem("username");
            admConnection.invoke("BackConnectedAsync", name);
        });
    }
    $('<li><a href="/">Вернуться в обычный режим</a></li>').appendTo('#dropcontextcontrolmenu');
});

admConnection.on("LoginSuccess", (response) => {
    console.log(response);
});

admConnection.on("LoginFail", (response) => {
    console.log(response);
});