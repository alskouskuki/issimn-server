﻿$('#loginbtn-1').click(function () {
    var name = $('#username-1').val();

    if (name === "") {
        $('#username-1').css('border-color', 'red');
    }
    else {
        $('#username-1').css('border-color', '');
        window.localStorage.setItem("username", name);
        window.location = "http://localhost:5002";
    }
});

$(document).ready(function () {
    if (window.localStorage.getItem("username") !== null) {
        window.location = "http://localhost:5002";
    }
})