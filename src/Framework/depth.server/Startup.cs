﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using AutoMapper;

using DE.DATABASELAYER.COMPONENT.Context;
using DE.WORKFLOWS.Hubs;
using DE.WORKFLOWS.Services.Users;
using INFRASTRUCTURE.LoadManagers;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Messages;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Sources;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Subs;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Users;
using INFRASTRUCTURE.Services.Users;

namespace depth.server
{
    public class Startup
    {
        private L01SystemLoader l01SystemLoader;

        private L02DataLoaderManager l02DataLoaderManager;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSignalR();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                {
                    builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithOrigins("http://localhost:5004")
                    .AllowCredentials();
                });
            });

            services.AddSingleton<IOpenUserService, OpenUserService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            /* START=LOADERS */

            this.l01SystemLoader = new L01SystemLoader();

            this.l02DataLoaderManager = new L02DataLoaderManager();

            /* END=LOADERS */
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseCors("CorsPolicy");

            app.UseSignalR(route =>
            {
                route.MapHub<OpenHub>("/openHub");
                route.MapHub<AdminHub>("/adminHub");
            });

            app.UseMvc();
        }
    }
}
