﻿const connection = new signalR.HubConnectionBuilder()
    .withUrl("http://dev.local:5000/openHub")
    .configureLogging(signalR.LogLevel.Information)
    .build();

let messages;
let messagesChannel;
let subscriptions;
let sources = [];
let sourceevents = [];
let sourceeventchannel;


$(document).ready(function () {
    connection.start().then(function () {
        connection.invoke("BackConnectedAsync", "superAlsko");
    });
});

connection.on("RestoreMessages",
    (response, response2) => {
        if (response === undefined) {
            console.log("MES_none");
        } else {
            messages = response;
        }
        if (response2 === undefined) {
            console.log("MES2_none");
        } else {
            messagesChannel = response2;
        }
    });

connection.on("RestoreSubscriptions",
    (response) => {
        if (response === undefined) {
            console.log("SUB_none");
        } else {
            subscriptions = response;
        }
    });

connection.on("ReceivingMessage",
    (response) => {
        if (response === undefined) {
            console.log("ERROR_getmessage");
        } else {
            messages.push(response);
        }
    });

connection.on("GetSourceChain",
    (response) => {
        if (response === undefined) {
            console.log("ERROR_getmessage");
        } else {
            sources.push(response);
        }
    });

connection.on("GetSourceEventsChain",
    (response) => {
        if (response === undefined) {
            console.log("ERROR_getmessage");
        } else {
            sourceevents.push(response);
        }
    });

connection.on("GetSourceEventChannelsChain",
    (response) => {
        if (response === undefined) {
            console.log("ERROR_getmessage");
        } else {
            sourceeventchannel = response;
            SetChannelToList();
        }
    });

function SetChannelToList() {

    for (let sub of subscriptions) {
        for (let channel of sourceeventchannel) {
            if (sub.chrn === channel.rn) {
                $('<div id=' + channel.rn +' class="chat_list" onclick="GetMessagesForConcreteChannel(id);">' +
                                '<div class= "chat_people" >' +
                                '<div class="chat_ib"> ' +
                            '<h5>' + channel.name + '</h5>' +
                                '</div>' +
                                '</div>' +
                                '</div >')
                    .appendTo('.inbox_chat');


            }
        }
    }
}

function GetMessagesForConcreteChannel(rn) {
    $('#MessagesHistory').html("");
    for (let messageChannel of messagesChannel) {
        if (messageChannel.chrn == rn) {
            for (let message of messages) {
                if (message.rn === messageChannel.msrn) {
                    $(' <div class="outcoming_msg">' +
                        '<div class="received_msg">' +
                            '<div class="received_withd_msg">' +
                            '<p>' + message.messagedata + '</p>' +
                                        '</div>' +
                        '</div>' +
                        '</div >').appendTo('#MessagesHistory');
                }
            }
        }
    }
}