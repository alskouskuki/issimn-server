﻿"use strict";
"use signalR";
const connection = new signalR.HubConnectionBuilder()
    .withUrl("http://dev.local:5000/openHub")
    .configureLogging(signalR.LogLevel.Information)
    .build();


$(document).ready(function() {
    connection.start().then(function() {
        connection.invoke("BackConnectedAsync", "superAlsko");
    });
});

connection.on("RestoreMessages",
    (response) => {
        if (response === undefined) {
            console.log("MES_none");
        } else {
            console.log(response);
        }
    });

connection.on("RestoreSubscriptions",
    (response) => {
        if (response === undefined) {
            console.log("SUB_none");
        } else {
            console.log(response);
        }
    });

connection.on("GetSourceEventChannelsChain",
    (response) => {
        if (response === undefined) {
            console.log("SEC_none");
        } else {
            console.log(response);
        }
    });
