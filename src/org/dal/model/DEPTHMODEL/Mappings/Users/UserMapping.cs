﻿using DEPTHMODEL.Domains.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DEPTHMODEL.Mappings.Users
{
    public class UserMapping
    {
        public UserMapping(ModelBuilder modelBuilder)
        {
            MapUser(modelBuilder);
            MapUserRights(modelBuilder);
        }

        private void MapUser(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("USER", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasColumnName("FULLNAME")
                    .HasColumnType("character varying(150)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("USERNAME")
                    .HasColumnType("character varying(40)");

                entity.Property(e => e.UserAccountType)
                    .HasColumnName("USERACCOUNTTYPE")
                    .HasColumnType("integer");
            });
        }

        private void MapUserRights(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Userrights>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("USERRIGHTS", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.CanCreateSource)
                    .IsRequired()
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanCreateUser)
                    .IsRequired()
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanDeleteSource)
                    .IsRequired()
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanDeleteUser)
                    .IsRequired()
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanUpdateSource)
                    .IsRequired()
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanUpdateUser)
                    .IsRequired()
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanSendMessages)
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CanUpdateMessages)
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Userrn).HasColumnName("USERRN");

                entity.HasOne(d => d.UserrnNavigation)
                    .WithMany(p => p.Userrights)
                    .HasForeignKey(d => d.Userrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("USERRIGHT_PRN_USER");
            });
        }
    }
}
