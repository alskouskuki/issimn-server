﻿using DEPTHMODEL.Domains.Subscriptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DEPTHMODEL.Mappings.Subs
{
    public class UserChannelSubscriptionMapping
    {
        public UserChannelSubscriptionMapping(ModelBuilder modelBuilder)
        {
            MapUserChannelSubscription(modelBuilder);
        }

        private void MapUserChannelSubscription(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Userhchannelsubscription>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("USERHCHANNELSUBSCRIPTION", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.Chrn).HasColumnName("CHRN");

                entity.Property(e => e.Usrn).HasColumnName("USRN");

                entity.HasOne(d => d.ChrnNavigation)
                    .WithMany(p => p.Userhchannelsubscription)
                    .HasForeignKey(d => d.Chrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("SUB_FK_CHRN");

                entity.HasOne(d => d.UsrnNavigation)
                    .WithMany(p => p.Userhchannelsubscription)
                    .HasForeignKey(d => d.Usrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("SUB_FK_USRN");
            });
        }
    }
}
