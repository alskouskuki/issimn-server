﻿using DEPTHMODEL.Domains.Sources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DEPTHMODEL.Mappings.Sources
{
    public class SourcesMapping
    {
        public SourcesMapping(ModelBuilder modelBuilder)
        {
            MapSource(modelBuilder);
            MapSourceEvent(modelBuilder);
            MapSourceEventChannel(modelBuilder);
        }

        private void MapSource(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Source>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("SOURCE", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Prn).HasColumnName("PRN");

                entity.Property(e => e.Sourcename)
                    .IsRequired()
                    .HasColumnName("SOURCENAME")
                    .HasColumnType("character varying(150)");

                entity.HasOne(d => d.PrnNavigation)
                    .WithMany(p => p.InversePrnNavigation)
                    .HasForeignKey(d => d.Prn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("SOURCE_FK_PRN");
            });
        }

        private void MapSourceEvent(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sourceevent>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("SOURCEEVENT", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasColumnType("character varying(150)");

                entity.Property(e => e.Prn).HasColumnName("PRN");

                entity.HasOne(d => d.PrnNavigation)
                    .WithMany(p => p.Sourceevent)
                    .HasForeignKey(d => d.Prn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("SOURCEEVENT_FK_PRN");
            });
        }

        private void MapSourceEventChannel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sourceeventchannel>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("SOURCEEVENTCHANNEL", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasColumnType("character varying(150)");

                entity.Property(e => e.Prn).HasColumnName("PRN");

                entity.HasOne(d => d.RnNavigation)
                    .WithOne(p => p.Sourceeventchannel)
                    .HasForeignKey<Sourceeventchannel>(d => d.Rn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CHANNEL_FK_PRN");
            });
        }
    }
}
