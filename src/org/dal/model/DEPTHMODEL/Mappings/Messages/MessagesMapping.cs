﻿using DEPTHMODEL.Domains.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DEPTHMODEL.Mappings.Messages
{
    public class MessagesMapping
    {
        public MessagesMapping(ModelBuilder modelBuilder)
        {
            MapMessages(modelBuilder);
            MapMessagesUser(modelBuilder);
            MapMessagesChannel(modelBuilder);
        }

        private void MapMessages(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("MESSAGE", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Importace)
                    .HasColumnName("IMPORTACE")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Messagedata)
                    .IsRequired()
                    .HasColumnName("MESSAGEDATA")
                    .HasColumnType("character varying(350)[]");

                entity.Property(e => e.Prn).HasColumnName("PRN");

                entity.HasOne(d => d.PrnNavigation)
                    .WithMany(p => p.Message)
                    .HasForeignKey(d => d.Prn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MESS_FK_PRN");
            });
        }

        private void MapMessagesUser(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Messageuser>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("MESSAGEUSER", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.Msrn).HasColumnName("MSRN");

                entity.Property(e => e.Read)
                    .HasColumnName("READ")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Usrn).HasColumnName("USRN");

                entity.HasOne(d => d.MsrnNavigation)
                    .WithMany(p => p.Messageuser)
                    .HasForeignKey(d => d.Msrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MSGUSER_FK_MSRN");

                entity.HasOne(d => d.UsrnNavigation)
                    .WithMany(p => p.Messageuser)
                    .HasForeignKey(d => d.Usrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MSGUSER_FK_USRN");
            });
        }

        private void MapMessagesChannel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Messagechannel>(entity =>
            {
                entity.HasKey(e => e.Rn);

                entity.ToTable("MESSAGECHANNEL", "depth");

                entity.Property(e => e.Rn)
                    .HasColumnName("RN")
                    .ValueGeneratedNever();

                entity.Property(e => e.Chrn).HasColumnName("CHRN");

                entity.Property(e => e.Msrn).HasColumnName("MSRN");

                entity.HasOne(d => d.ChrnNavigation)
                    .WithMany(p => p.Messagechannel)
                    .HasForeignKey(d => d.Chrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MSGCNHL_FK_CHRN");

                entity.HasOne(d => d.MsrnNavigation)
                    .WithMany(p => p.Messagechannel)
                    .HasForeignKey(d => d.Msrn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MSGCHNL_FK_MSRN");
            });
        }
    }
}
