﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DEPTHMODEL.Domains.Sources
{
    public partial class Sourceevent
    {
        public int Rn { get; set; }
        public string Name { get; set; }
        public int Prn { get; set; }
        public DateTime? CreationDate { get; set; }

        [JsonIgnore]
        public Source PrnNavigation { get; set; }

        public Sourceeventchannel Sourceeventchannel { get; set; }
    }
}
