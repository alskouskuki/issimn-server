﻿using DEPTHMODEL.Domains.Messages;
using DEPTHMODEL.Domains.Subscriptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DEPTHMODEL.Domains.Sources
{
    public partial class Sourceeventchannel
    {
        public Sourceeventchannel()
        {
            Messagechannel = new HashSet<Messagechannel>();
            Userhchannelsubscription = new HashSet<Userhchannelsubscription>();
        }

        public int Rn { get; set; }
        public string Name { get; set; }
        public int Prn { get; set; }
        public DateTime? CreationDate { get; set; }

        [JsonIgnore]
        public Sourceevent RnNavigation { get; set; }

        [JsonIgnore]
        public ICollection<Messagechannel> Messagechannel { get; set; }

        [JsonIgnore]
        public ICollection<Userhchannelsubscription> Userhchannelsubscription { get; set; }
    }
}
