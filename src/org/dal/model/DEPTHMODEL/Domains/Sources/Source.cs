﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DEPTHMODEL.Domains.Sources
{
    public partial class Source
    {
        public Source()
        {
            InversePrnNavigation = new HashSet<Source>();
            Sourceevent = new HashSet<Sourceevent>();
        }

        public int Rn { get; set; }
        public string Sourcename { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? Prn { get; set; }

        public Source PrnNavigation { get; set; }

        public ICollection<Source> InversePrnNavigation { get; set; }

        public ICollection<Sourceevent> Sourceevent { get; set; }
    }
}
