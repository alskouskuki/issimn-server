﻿using DEPTHMODEL.Domains.Sources;
using DEPTHMODEL.Domains.Users;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DEPTHMODEL.Domains.Subscriptions
{
    public partial class Userhchannelsubscription
    {
        public int Rn { get; set; }
        public int Usrn { get; set; }
        public int Chrn { get; set; }

        public Sourceeventchannel ChrnNavigation { get; set; }

        public User UsrnNavigation { get; set; }
    }
}
