﻿using DEPTHMODEL.Domains.Messages;
using DEPTHMODEL.Domains.Subscriptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DEPTHMODEL.Domains.Users
{
    public partial class User
    {
        public User()
        {
            Message = new HashSet<Message>();
            Messageuser = new HashSet<Messageuser>();
            Userhchannelsubscription = new HashSet<Userhchannelsubscription>();
        }

        public int Rn { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public int UserAccountType { get; set; }

        [JsonIgnore]
        public ICollection<Userrights> Userrights { get; set; }

        [JsonIgnore]
        public ICollection<Message> Message { get; set; }
        [JsonIgnore]
        public ICollection<Messageuser> Messageuser { get; set; }
        [JsonIgnore]
        public ICollection<Userhchannelsubscription> Userhchannelsubscription { get; set; }
    }
}
