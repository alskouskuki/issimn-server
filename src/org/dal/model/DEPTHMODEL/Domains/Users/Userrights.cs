﻿using Newtonsoft.Json;
using System.Collections;

namespace DEPTHMODEL.Domains.Users
{
    public partial class Userrights
    {
        public int Rn { get; set; }
        public int Userrn { get; set; }
        public BitArray CanCreateSource { get; set; }
        public BitArray CanCreateUser { get; set; }
        public BitArray CanUpdateSource { get; set; }
        public BitArray CanUpdateUser { get; set; }
        public BitArray CanDeleteSource { get; set; }
        public BitArray CanDeleteUser { get; set; }
        public BitArray CanSendMessages { get; set; }
        public BitArray CanUpdateMessages { get; set; }

        public User UserrnNavigation { get; set; }
    }
}
