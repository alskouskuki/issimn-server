﻿using DEPTHMODEL.Domains.Sources;
using Newtonsoft.Json;

namespace DEPTHMODEL.Domains.Messages
{
    public partial class Messagechannel
    {
        public int Rn { get; set; }
        public int Msrn { get; set; }
        public int Chrn { get; set; }

        [JsonIgnore]
        public Sourceeventchannel ChrnNavigation { get; set; }

        [JsonIgnore]
        public Message MsrnNavigation { get; set; }
    }
}
