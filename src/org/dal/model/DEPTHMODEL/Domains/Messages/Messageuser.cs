﻿using DEPTHMODEL.Domains.Users;
using Newtonsoft.Json;
using System.Collections;

namespace DEPTHMODEL.Domains.Messages
{
    public partial class Messageuser
    {
        public int Rn { get; set; }
        public int Msrn { get; set; }
        public int Usrn { get; set; }
        public BitArray Read { get; set; }

        [JsonIgnore]
        public Message MsrnNavigation { get; set; }

        [JsonIgnore]
        public User UsrnNavigation { get; set; }
    }
}
