﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DEPTHMODEL.Domains.Users;
using Newtonsoft.Json;

namespace DEPTHMODEL.Domains.Messages
{
    public class Message
    {
        public Message()
        {
            Messagechannel = new HashSet<Messagechannel>();
            Messageuser = new HashSet<Messageuser>();
        }

        [Key]
        public int Rn { get; set; }
        public string Messagedata { get; set; }
        public int Prn { get; set; }
        public BitArray Importace { get; set; }
        public DateTime? CreationDate { get; set; }

        [JsonIgnore] public User PrnNavigation { get; set; }

        [JsonIgnore] public ICollection<Messagechannel> Messagechannel { get; set; }

        [JsonIgnore] public ICollection<Messageuser> Messageuser { get; set; }
    }
}