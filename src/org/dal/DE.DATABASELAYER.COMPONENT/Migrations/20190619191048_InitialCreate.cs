﻿using System;
using System.Collections;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DE.DATABASELAYER.COMPONENT.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "depth");

            migrationBuilder.CreateTable(
                name: "SOURCE",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    SOURCENAME = table.Column<string>(type: "character varying(150)", nullable: false),
                    CREATION_DATE = table.Column<DateTime>(type: "date", nullable: true),
                    PRN = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SOURCE", x => x.RN);
                    table.ForeignKey(
                        name: "SOURCE_FK_PRN",
                        column: x => x.PRN,
                        principalSchema: "depth",
                        principalTable: "SOURCE",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USER",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    USERNAME = table.Column<string>(type: "character varying(40)", nullable: false),
                    FULLNAME = table.Column<string>(type: "character varying(150)", nullable: false),
                    USERACCOUNTTYPE = table.Column<int>(type: "integer", nullable: false),
                    FUSHION = table.Column<string>(type: "character varying(40)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USER", x => x.RN);
                });

            migrationBuilder.CreateTable(
                name: "SOURCEEVENT",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    NAME = table.Column<string>(type: "character varying(150)", nullable: false),
                    PRN = table.Column<int>(nullable: false),
                    CREATION_DATE = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SOURCEEVENT", x => x.RN);
                    table.ForeignKey(
                        name: "SOURCEEVENT_FK_PRN",
                        column: x => x.PRN,
                        principalSchema: "depth",
                        principalTable: "SOURCE",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MESSAGE",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    MESSAGEDATA = table.Column<string>(type: "character varying(350)[]", nullable: false),
                    PRN = table.Column<int>(nullable: false),
                    IMPORTACE = table.Column<BitArray>(type: "bit(1)", nullable: true),
                    CREATION_DATE = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MESSAGE", x => x.RN);
                    table.ForeignKey(
                        name: "MESS_FK_PRN",
                        column: x => x.PRN,
                        principalSchema: "depth",
                        principalTable: "USER",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USERRIGHTS",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    USERRN = table.Column<int>(nullable: false),
                    CanCreateSource = table.Column<BitArray>(type: "bit(1)", nullable: false),
                    CanCreateUser = table.Column<BitArray>(type: "bit(1)", nullable: false),
                    CanUpdateSource = table.Column<BitArray>(type: "bit(1)", nullable: false),
                    CanUpdateUser = table.Column<BitArray>(type: "bit(1)", nullable: false),
                    CanDeleteSource = table.Column<BitArray>(type: "bit(1)", nullable: false),
                    CanDeleteUser = table.Column<BitArray>(type: "bit(1)", nullable: false),
                    CanSendMessages = table.Column<BitArray>(type: "bit(1)", nullable: true),
                    CanUpdateMessages = table.Column<BitArray>(type: "bit(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USERRIGHTS", x => x.RN);
                    table.ForeignKey(
                        name: "USERRIGHT_PRN_USER",
                        column: x => x.USERRN,
                        principalSchema: "depth",
                        principalTable: "USER",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SOURCEEVENTCHANNEL",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    NAME = table.Column<string>(type: "character varying(150)", nullable: false),
                    PRN = table.Column<int>(nullable: false),
                    CREATION_DATE = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SOURCEEVENTCHANNEL", x => x.RN);
                    table.ForeignKey(
                        name: "CHANNEL_FK_PRN",
                        column: x => x.RN,
                        principalSchema: "depth",
                        principalTable: "SOURCEEVENT",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MESSAGEUSER",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    MSRN = table.Column<int>(nullable: false),
                    USRN = table.Column<int>(nullable: false),
                    READ = table.Column<BitArray>(type: "bit(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MESSAGEUSER", x => x.RN);
                    table.ForeignKey(
                        name: "MSGUSER_FK_MSRN",
                        column: x => x.MSRN,
                        principalSchema: "depth",
                        principalTable: "MESSAGE",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "MSGUSER_FK_USRN",
                        column: x => x.USRN,
                        principalSchema: "depth",
                        principalTable: "USER",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MESSAGECHANNEL",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    MSRN = table.Column<int>(nullable: false),
                    CHRN = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MESSAGECHANNEL", x => x.RN);
                    table.ForeignKey(
                        name: "MSGCNHL_FK_CHRN",
                        column: x => x.CHRN,
                        principalSchema: "depth",
                        principalTable: "SOURCEEVENTCHANNEL",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "MSGCHNL_FK_MSRN",
                        column: x => x.MSRN,
                        principalSchema: "depth",
                        principalTable: "MESSAGE",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USERHCHANNELSUBSCRIPTION",
                schema: "depth",
                columns: table => new
                {
                    RN = table.Column<int>(nullable: false),
                    USRN = table.Column<int>(nullable: false),
                    CHRN = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USERHCHANNELSUBSCRIPTION", x => x.RN);
                    table.ForeignKey(
                        name: "SUB_FK_CHRN",
                        column: x => x.CHRN,
                        principalSchema: "depth",
                        principalTable: "SOURCEEVENTCHANNEL",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "SUB_FK_USRN",
                        column: x => x.USRN,
                        principalSchema: "depth",
                        principalTable: "USER",
                        principalColumn: "RN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MESSAGE_PRN",
                schema: "depth",
                table: "MESSAGE",
                column: "PRN");

            migrationBuilder.CreateIndex(
                name: "IX_MESSAGECHANNEL_CHRN",
                schema: "depth",
                table: "MESSAGECHANNEL",
                column: "CHRN");

            migrationBuilder.CreateIndex(
                name: "IX_MESSAGECHANNEL_MSRN",
                schema: "depth",
                table: "MESSAGECHANNEL",
                column: "MSRN");

            migrationBuilder.CreateIndex(
                name: "IX_MESSAGEUSER_MSRN",
                schema: "depth",
                table: "MESSAGEUSER",
                column: "MSRN");

            migrationBuilder.CreateIndex(
                name: "IX_MESSAGEUSER_USRN",
                schema: "depth",
                table: "MESSAGEUSER",
                column: "USRN");

            migrationBuilder.CreateIndex(
                name: "IX_SOURCE_PRN",
                schema: "depth",
                table: "SOURCE",
                column: "PRN");

            migrationBuilder.CreateIndex(
                name: "IX_SOURCEEVENT_PRN",
                schema: "depth",
                table: "SOURCEEVENT",
                column: "PRN");

            migrationBuilder.CreateIndex(
                name: "IX_USERHCHANNELSUBSCRIPTION_CHRN",
                schema: "depth",
                table: "USERHCHANNELSUBSCRIPTION",
                column: "CHRN");

            migrationBuilder.CreateIndex(
                name: "IX_USERHCHANNELSUBSCRIPTION_USRN",
                schema: "depth",
                table: "USERHCHANNELSUBSCRIPTION",
                column: "USRN");

            migrationBuilder.CreateIndex(
                name: "IX_USERRIGHTS_USERRN",
                schema: "depth",
                table: "USERRIGHTS",
                column: "USERRN");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MESSAGECHANNEL",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "MESSAGEUSER",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "USERHCHANNELSUBSCRIPTION",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "USERRIGHTS",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "MESSAGE",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "SOURCEEVENTCHANNEL",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "USER",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "SOURCEEVENT",
                schema: "depth");

            migrationBuilder.DropTable(
                name: "SOURCE",
                schema: "depth");
        }
    }
}
