﻿using System;
using DEPTHMODEL.Domains.Messages;
using DEPTHMODEL.Domains.Sources;
using DEPTHMODEL.Domains.Subscriptions;
using DEPTHMODEL.Domains.Users;
using DEPTHMODEL.Mappings.Messages;
using DEPTHMODEL.Mappings.Sources;
using DEPTHMODEL.Mappings.Subs;
using DEPTHMODEL.Mappings.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace DE.DATABASELAYER.COMPONENT.Context
{
    public partial class developContext : DbContext
    {
        private UserMapping userMapping;

        private UserChannelSubscriptionMapping subscriptionMapping;

        private SourcesMapping sourceMapping;

        private MessagesMapping messagesMapping;

        public developContext()
        {
        }

        public developContext(DbContextOptions<developContext> options)
            : base(options)
        {
        }

        [Obsolete]
        public static readonly LoggerFactory MyLoggerFactory
            = new LoggerFactory(new[] { new ConsoleLoggerProvider((_, __) => true, true) });

        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Messagechannel> Messagechannel { get; set; }
        public virtual DbSet<Messageuser> Messageuser { get; set; }
        public virtual DbSet<Source> Source { get; set; }
        public virtual DbSet<Sourceevent> Sourceevent { get; set; }
        public virtual DbSet<Sourceeventchannel> Sourceeventchannel { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Userhchannelsubscription> Userhchannelsubscription { get; set; }
        public virtual DbSet<Userrights> Userrights { get; set; }

        [Obsolete]
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#pragma warning disable CS1030 // #warning directive
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder
#pragma warning restore CS1030 // #warning directive

#if !RELEASE
                    //.UseLoggerFactory(MyLoggerFactory)
#endif
                    .UseNpgsql("Host=localhost;Database=develop;Username=postgres;Password=DearGagatun1");
            }
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("depth");
            userMapping = new UserMapping(modelBuilder);
            subscriptionMapping = new UserChannelSubscriptionMapping(modelBuilder);
            messagesMapping = new MessagesMapping(modelBuilder);
            sourceMapping = new SourcesMapping(modelBuilder);
        }
    }
}
