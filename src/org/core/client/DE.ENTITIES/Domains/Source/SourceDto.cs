﻿using System;
using System.Collections.Generic;
using AutoMapper;

namespace DE.ENTITIES.Domains.Source
{
    public class SourceDto
    {
        public SourceDto()
        {
            InversePrnNavigation = new HashSet<SourceDto>();
            Sourceevent = new HashSet<SourceeventDto>();
        }

        public int Rn { get; set; }
        public string Sourcename { get; set; }
        public DateTime CreationDate { get; set; }
        public int Prn { get; set; }

        public SourceDto PrnNavigation { get; set; }
        public ICollection<SourceDto> InversePrnNavigation { get; set; }
        public ICollection<SourceeventDto> Sourceevent { get; set; }
    }
}
