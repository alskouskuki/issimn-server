﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DE.ENTITIES.Domains.Source
{
    public class SourceeventDto
    {
        public int Rn { get; set; }
        public string Name { get; set; }
        public int Prn { get; set; }
        public DateTime CreationDate { get; set; }

        public SourceDto PrnNavigation { get; set; }
        public SourceEventChannelDto Sourceeventchannel { get; set; }
    }
}
