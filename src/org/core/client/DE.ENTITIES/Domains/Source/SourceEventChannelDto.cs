﻿using DE.ENTITIES.Domains.Messages;
using DE.ENTITIES.Domains.Subs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DE.ENTITIES.Domains.Source
{
    public class SourceEventChannelDto
    {
        public SourceEventChannelDto()
        {
            Messagechannel = new HashSet<MessageChannelDto>();
            Userhchannelsubscription = new HashSet<UserchannelsubscriptionDto>();
        }

        public int Rn { get; set; }
        public string Name { get; set; }
        public int Prn { get; set; }
        public DateTime CreationDate { get; set; }

        public SourceeventDto RnNavigation { get; set; }
        public ICollection<MessageChannelDto> Messagechannel { get; set; }
        public ICollection<UserchannelsubscriptionDto> Userhchannelsubscription { get; set; }
    }
}
