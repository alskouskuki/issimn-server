﻿using System.Collections;

namespace DE.ENTITIES.Domains.Users
{
    public class UserRightsDto
    {
        public int Rn { get; set; }
        public int Userrn { get; set; }
        public BitArray CanCreateSource { get; set; }
        public BitArray CanCreateUser { get; set; }
        public BitArray CanUpdateSource { get; set; }
        public BitArray CanUpdateUser { get; set; }
        public BitArray CanDeleteSource { get; set; }
        public BitArray CanDeleteUser { get; set; }
        public BitArray CanCreateMessage { get; set; }
        public BitArray CanUpdateMessage { get; set; }

        public UserDto UserrnNavigation { get; set; }
    }
}
