﻿using DE.ENTITIES.Domains.Messages;
using DE.ENTITIES.Domains.Subs;
using System;
using System.Collections.Generic;

namespace DE.ENTITIES.Domains.Users
{
    public class UserDto
    {
        public UserDto()
        {
            Message = new HashSet<MessageDto>();
            Messageuser = new HashSet<MessageUserDto>();
            Userhchannelsubscription = new HashSet<UserchannelsubscriptionDto>();
            UserRights = new HashSet<UserRightsDto>();
        }

        public int Rn { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public int UserAccountType { get; set; }

        public ICollection<UserRightsDto> UserRights { get; set; } 
        public ICollection<MessageDto> Message { get; set; }
        public ICollection<MessageUserDto> Messageuser { get; set; }
        public ICollection<UserchannelsubscriptionDto> Userhchannelsubscription { get; set; }
    }
}
