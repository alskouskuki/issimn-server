﻿using System;
using System.Collections;
using System.Collections.Generic;
using DE.ENTITIES.Domains.Users;
using Newtonsoft.Json;

namespace DE.ENTITIES.Domains.Messages
{
    public class MessageDto
    {
        public MessageDto()
        {
            Messagechannel = new HashSet<MessageChannelDto>();
            Messageuser = new HashSet<MessageUserDto>();
        }

        public int Rn { get; set; }
        public string Messagedata { get; set; }
        public int Prn { get; set; }
        public BitArray Importace { get; set; }
        public DateTime? CreationDate { get; set; }

        [JsonIgnore] public UserDto PrnNavigation { get; set; }

        [JsonIgnore] public ICollection<MessageChannelDto> Messagechannel { get; set; }

        [JsonIgnore] public ICollection<MessageUserDto> Messageuser { get; set; }
    }
}