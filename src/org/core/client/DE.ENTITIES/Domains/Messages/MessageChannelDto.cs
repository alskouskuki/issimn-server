﻿using DE.ENTITIES.Domains.Source;
using System;
using System.Collections.Generic;
using System.Text;

namespace DE.ENTITIES.Domains.Messages
{
    public class MessageChannelDto
    {
        public int Rn { get; set; }
        public int Msrn { get; set; }
        public int Chrn { get; set; }

        public SourceEventChannelDto ChrnNavigation { get; set; }
        public MessageDto MsrnNavigation { get; set; }
    }
}
