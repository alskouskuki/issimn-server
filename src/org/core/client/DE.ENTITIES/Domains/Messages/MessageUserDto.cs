﻿using DE.ENTITIES.Domains.Users;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DE.ENTITIES.Domains.Messages
{
    public class MessageUserDto
    {
        public int Rn { get; set; }
        public int Msrn { get; set; }
        public int Usrn { get; set; }
        public BitArray Read { get; set; }

        public MessageDto MsrnNavigation { get; set; }
        public UserDto UsrnNavigation { get; set; }
    }
}
