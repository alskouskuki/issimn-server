﻿using DE.ENTITIES.Domains.Source;
using DE.ENTITIES.Domains.Users;
using Newtonsoft.Json;

namespace DE.ENTITIES.Domains.Subs
{
    public class UserchannelsubscriptionDto
    {
        public int Rn { get; set; }
        public int Usrn { get; set; }
        public int Chrn { get; set; }

        [JsonIgnore]
        public SourceEventChannelDto ChrnNavigation { get; set; }
        [JsonIgnore]
        public UserDto UsrnNavigation { get; set; }
    }
}
