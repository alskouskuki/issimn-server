﻿using DE.DATABASELAYER.COMPONENT.Context;
using DE.ENTITIES.Domains.Source;
using INFRASTRUCTURE.Services.Source;
using System;
using AutoMapper;
using DEPTHMODEL.Domains.Sources;
using Microsoft.EntityFrameworkCore;

namespace DE.WORKFLOWS.Services.Source
{
    public class SourceManipulationService : ISourceManipulationService
    {
        public void CreateSource(string name, SourceDto source = null)
        {
            using (var db = new developContext())
            {
                if (source != null)
                {
                    var newEntity = new SourceDto
                    {
                        Rn = new Guid().GetHashCode(),
                        Sourcename = name,
                        CreationDate = DateTime.Now,
                        Prn = source.Rn,
                        PrnNavigation = source
                    };
                    db.Source.Add(Mapper.Map<DEPTHMODEL.Domains.Sources.Source>(newEntity));
                }

                if (source == null)
                {
                    var newEntity = new SourceDto
                    {
                        Rn = new Guid().GetHashCode(),
                        Sourcename = name,
                        CreationDate = DateTime.Now
                    };
                    db.Source.Add(Mapper.Map<DEPTHMODEL.Domains.Sources.Source>(newEntity));
                }

                db.Database.CommitTransaction();
            }
        }

        public void CreateSourceEvent(string name, SourceDto source)
        {
            using(var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                var entity = new SourceeventDto
                {
                    Rn = new Guid().GetHashCode(),
                    Name = name,
                    Prn = source.Rn,
                    CreationDate = DateTime.Now
                };

                db.Sourceevent.Add(Mapper.Map<Sourceevent>(entity));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void CreateSourceEventChannel(string name, SourceeventDto sourceEvent)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();

                var entity = new SourceEventChannelDto
                {
                    Rn = new Guid().GetHashCode(),
                    Name = name,
                    Prn = sourceEvent.Rn,
                    CreationDate = DateTime.Now
                };
                db.Sourceeventchannel.Add(Mapper.Map<Sourceeventchannel>(entity));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void DeleteSource(SourceDto source)
        {
            using(var db = new developContext())
            {
                db.Source.Remove(Mapper.Map<DEPTHMODEL.Domains.Sources.Source>(source));
                db.Database.CommitTransaction();
            }
        }

        public void DeleteSourceEvent(SourceeventDto sourceEvent)
        {
            if (sourceEvent == null) throw new ArgumentNullException(nameof(sourceEvent));
            using (var db = new developContext())
            {
                db.Sourceevent.Remove(Mapper.Map<Sourceevent>(sourceEvent));
                db.Database.CommitTransaction();
            }
        }

        public void DeleteSourceEventChannel(SourceEventChannelDto sourceEventChannel)
        {
            using (var db = new developContext())
            {
                db.Sourceeventchannel.Remove(Mapper.Map<Sourceeventchannel>(sourceEventChannel));
                db.Database.CommitTransaction();
            }
        }

        public void UpdateSource(SourceDto source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Source.Update(Mapper.Map<DEPTHMODEL.Domains.Sources.Source>(source));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void UpdateSourceEvent(SourceeventDto sourceEvent)
        {
            if (sourceEvent == null) throw new ArgumentNullException(nameof(sourceEvent));
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Sourceevent.Update(Mapper.Map<Sourceevent>(sourceEvent));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void UpdateSourceEventChannel(SourceEventChannelDto sourceEventChannel)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Sourceeventchannel.Update(Mapper.Map<Sourceeventchannel>(sourceEventChannel));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void Dispose()
        {
        }
    }
}
