﻿using DE.DATABASELAYER.COMPONENT.Context;
using DE.ENTITIES.Domains.Users;
using INFRASTRUCTURE.Services.Users;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DE.ENTITIES.Domains.Messages;
using DE.ENTITIES.Domains.Source;
using DE.ENTITIES.Domains.Subs;
using DEPTHMODEL.Domains.Subscriptions;
using DEPTHMODEL.Domains.Users;

namespace DE.WORKFLOWS.Services.Users
{
    public class OpenUserService : IOpenUserService
    {
        public IList<UserRightsDto> GetRightsOfUser(IList<UserDto> user)
        {
           var repercussion = (user.Select(x => x.UserRights).FirstOrDefault() ?? throw new InvalidOperationException()).ToList();

            return repercussion;
        }

        public IList<UserDto> GetUserByName(string username)
        {
            using(var db = new developContext())
            {
                var user = db.User.Where(x => x.Username == username).ToList();

                return Mapper.Map<IList<UserDto>>(user);
            }
        }

        public void CreateSubscription(UserDto user, SourceEventChannelDto sourceEventChannel)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                var entity = new UserchannelsubscriptionDto
                {
                    Rn = new Guid().GetHashCode(),
                    Chrn = sourceEventChannel.Rn,
                    Usrn = user.Rn
                };
                db.Userhchannelsubscription.Add(Mapper.Map<Userhchannelsubscription>(entity));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void UpdateSubscription(UserchannelsubscriptionDto userChannelSubscription)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Userhchannelsubscription.Update(Mapper.Map<Userhchannelsubscription>(userChannelSubscription));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void DeleteSubscription(UserchannelsubscriptionDto userChannelSubscription)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Userhchannelsubscription.Remove(Mapper.Map<Userhchannelsubscription>(userChannelSubscription));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void Dispose()
        {
        }
    }

    public class AdminUserService : IAdminUserService
    {
        public void CreateUser(string username, string fullname, int accountType)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                var entity = new UserDto
                {
                    Rn = new Guid().GetHashCode(),
                    Username = username,
                    Fullname = fullname,
                    UserAccountType = accountType
                };
                db.User.Add(Mapper.Map<User>(entity));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();

            }
        }

        public void AddPermissionsToUser(UserRightsDto userRights)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();

                var entity = Mapper.Map<Userrights>(userRights);

                db.Userrights.Add(entity);
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void DeleteUser(UserDto user)
        {
            using (var db = new developContext())
            {
                db.User.Remove(Mapper.Map<User>(user));
                db.Database.CommitTransaction();
            }
        }

        public void DeleteUserRights(UserRightsDto userRights)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Userrights.Remove(Mapper.Map<Userrights>(userRights));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void UpdateRightsToUser(UserRightsDto userRights)
        {
            using (var db = new developContext())
            {
                db.Database.OpenConnectionAsync();
                db.Userrights.Update(Mapper.Map<Userrights>(userRights));
                db.Database.CommitTransaction();
                db.Database.CloseConnection();
            }
        }

        public void UpdateUser(UserDto user)
        {
            using (var db = new developContext())
            {
                db.User.Update(Mapper.Map<User>(user));
                db.Database.CommitTransaction();
            }
        }

        public void Dispose()
        { 
        }
    }
}
