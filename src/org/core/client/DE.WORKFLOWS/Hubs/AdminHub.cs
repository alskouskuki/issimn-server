﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DE.DATABASELAYER.COMPONENT.Context;
using DE.ENTITIES.Domains.Source;
using DE.ENTITIES.Domains.Users;
using DE.WORKFLOWS.Services.Source;
using DE.WORKFLOWS.Services.Users;
using INFRASTRUCTURE;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;

namespace DE.WORKFLOWS.Hubs
{
    public class AdminHub : CustomHub
    {
        private readonly IApplicationLifetime _applicationLifetime;

        public AdminHub(IApplicationLifetime applicationLifetime)
        {
            _applicationLifetime = applicationLifetime;
        }

        public Task CreateSource(string sourceName)
        {
            using (var service = new SourceManipulationService())
            {
                service.Dispose();
                service.CreateSource(sourceName);
            }

            Debug.WriteLine("Created");

            return Task.CompletedTask;
        }

        public Task CreateSourceEvent(string sourceEventName, SourceDto source)
        {
            using (var service = new SourceManipulationService())
            {
                service.CreateSourceEvent(sourceEventName, source);
            }

            Debug.Write("Created");

            return Task.CompletedTask;
        }

        public Task CreateSourceEventChannel(string sourceEventChannelName, SourceeventDto sourceEvent)
        {
            using (var service = new SourceManipulationService())
            {
                service.CreateSourceEventChannel(sourceEventChannelName, sourceEvent);
            }

            return Task.CompletedTask;
        }

        public Task CreateUser(string username, string fullname, int accountType)
        {
            using (var service = new AdminUserService())
            {
                service.CreateUser(username, fullname, accountType);
            }

            return Task.CompletedTask;
        }

        public Task AddPermissionsForUser(UserDto user, List<BitArray> permissions)
        {
            using (var service = new AdminUserService())
            {
                // service.AddPermissionsToUser();
            }

            return Task.CompletedTask;
        }

        /* END=CREATION_METHODS */

        /* TODO START=UPDATE_METHODS */

        public Task UpdateSource(SourceDto source)
        {
            using (var service = new SourceManipulationService())
            {
                service.DeleteSource(source);
            }

            return Task.CompletedTask;
        }

        public Task UpdateSourceEvent(SourceeventDto sourceEvent)
        {
            using (var service = new SourceManipulationService())
            {
                service.UpdateSourceEvent(sourceEvent);
            }

            return Task.CompletedTask;
        }

        public Task UpdateSorceEventChannel(SourceEventChannelDto sourceEventChannel)
        {
            using (var service = new SourceManipulationService())
            {
                service.UpdateSourceEventChannel(sourceEventChannel);
            }

            return Task.CompletedTask;
        }

        public Task UpdatesUser(UserDto user)
        {
            using (var service = new AdminUserService())
            {
                service.UpdateUser(user);
            }

            return Task.CompletedTask;
        }

        public Task UpdatePermissions(UserRightsDto userRights)
        {
            using (var service = new AdminUserService())
            {
                service.UpdateRightsToUser(userRights);
            }

            return Task.CompletedTask;
        }

        /* END=UPDATE_METHODS */

        /* TODO START=DELETE_METHODS */

        public Task DeleteSource(SourceDto source)
        {
            using (var service = new SourceManipulationService())
            {
                service.DeleteSource(source);
            }

            return Task.CompletedTask;
        }

        public Task DeleteSourceEvent(SourceeventDto sourceEvent)
        {
            using (var service = new SourceManipulationService())
            {
                service.DeleteSourceEvent(sourceEvent);
            }

            return Task.CompletedTask;
        }

        public Task DeleteSourceEventChannel(SourceEventChannelDto sourceEventChannel)
        {
            using (var service = new SourceManipulationService())
            {
                service.DeleteSourceEventChannel(sourceEventChannel);
            }

            return Task.CompletedTask;
        }

        public Task DeleteUser(UserDto user)
        {
            using (var service = new AdminUserService())
            {
                service.DeleteUser(user);
            }

            return Task.CompletedTask;
        }

        public Task DeleteUserPermission(UserRightsDto userRights)
        {
            using (var service = new AdminUserService())
            {
                service.DeleteUserRights(userRights);
            }

            return Task.CompletedTask;
        }

        public void ShutdownServer()
        {
            _applicationLifetime.StopApplication();
        }

        /* END=DELETE_METHODS AND ALL MANIPULATION METHODS */

        public override async Task BackConnectedAsync(string username)
        {
            await CheckPermissionsForUser(username);
        }

        private async Task CheckPermissionsForUser(string username)
        {
            using (var db = new developContext())
            {
                var havePermissions = db.Userrights.Any(x => x.UserrnNavigation.Username == username);

                if (havePermissions)
                {
                    var listOfPermissions = db.Userrights.Where(x => x.UserrnNavigation.Username == username).ToList();

                    await Clients.Caller.SendAsync("LoginSuccess", listOfPermissions);
                }
                else
                {
                    await Clients.Caller.SendAsync("LoginFail", 401);
                }
            }
        }
    }
}