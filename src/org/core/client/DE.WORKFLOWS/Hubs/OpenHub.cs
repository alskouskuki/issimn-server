﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DE.DATABASELAYER.COMPONENT.Context;
using DE.ENTITIES.Domains.Messages;
using DE.ENTITIES.Domains.Source;
using DE.ENTITIES.Domains.Subs;
using DE.ENTITIES.Domains.Users;
using DE.WORKFLOWS.Managers.Messages;
using DE.WORKFLOWS.Services.Users;
using DEPTHMODEL.Domains.Users;
using INFRASTRUCTURE;
using INFRASTRUCTURE.Context.User;
using INFRASTRUCTURE.Mapping;
using Microsoft.AspNetCore.SignalR;

namespace DE.WORKFLOWS.Hubs
{
    public class OpenHub : CustomHub
    {
        private static readonly ConnectionMapping<string> connection = new ConnectionMapping<string>();

        private static readonly UserContext userContext = new UserContext();

        private static readonly NewestMessageManager newestMessageManager = new NewestMessageManager();
        private OpenUserService userService = new OpenUserService();

        public override Task OnDisconnectedAsync(Exception exception)
        {
            if (userContext.UserName != null) connection.Remove(userContext.UserName, userContext.ConnectingId);

            return base.OnDisconnectedAsync(exception);
        }

        public override async Task BackConnectedAsync(string username)
        {
            if (!string.IsNullOrEmpty(username))
                await CheckLoggingUser(username);
            else
                await Clients.Caller.SendAsync("Error", null);
        }

        public async Task UpdateOrAddGroup(string groupName)
        {
            await Groups.AddToGroupAsync(UserContext.ConnectingId, groupName);
        }

        public async Task SendPrivateMessage(string groupName, string username, MessageDto message)
        {
            await Clients.Group(groupName).SendAsync("SendToGroup",
                Clients.User(username).SendAsync("InGroupSendingPrivate", UserContext.UserName, message));
        }

        public async Task CreateSubscriptionForUser(SourceEventChannelDto sourceEventChannelDto)
        {
            using (var service = new OpenUserService())
            {
                using (var db = new developContext())
                {
                    var user = db.User.FirstOrDefault(x => x.Username == UserContext.UserName);

                    service.CreateSubscription(Mapper.Map<UserDto>(user), sourceEventChannelDto);
                }
            }
        }

        private async Task CheckLocalMessagesForConcreteUser(IEnumerable<User> user, developContext db)
        {
            var _user = user.Select(x => x.Rn).FirstOrDefault();

            IList<MessageDto> messages = new List<MessageDto>();

            var newestMessages = newestMessageManager.MessageDtos.Select(x => x)
                .Where(x => x.Messageuser.Select(d => d.Usrn).FirstOrDefault() == _user).ToList();

            var oldMessages = db.Message.Select(x => x)
                .Where(x => x.Messageuser.Select(d => d.Usrn).FirstOrDefault() == _user).ToList();

            var oldMessagesChannel = db.Message.SelectMany(x => x.Messagechannel)
                .Where(x => x.MsrnNavigation.Messageuser.Select(d => d.Usrn).FirstOrDefault() == _user).ToList();

            if (newestMessages.Count() != 0)
                foreach (var item in newestMessages)
                    messages.Add(item);

            if (oldMessages.Count() != 0)
            {
                var list = Mapper.Map<IList<MessageDto>>(oldMessages);

                foreach (var item in list) messages.Add(item);
            }

            if (messages.Count != 0) await Clients.Caller.SendAsync("RestoreMessages", messages, oldMessagesChannel);
        }

        private async Task CheckSubscriptionsForUser(IEnumerable<User> user, developContext db)
        {
            var localUserRn = user.Select(x => x.Rn).FirstOrDefault();

            var subsOnSourceEventChannel = db.Userhchannelsubscription.Where(x => x.Usrn == localUserRn);

            if (subsOnSourceEventChannel.Count() != 0)
                try
                {
                    await Clients.Caller.SendAsync("RestoreSubscriptions", subsOnSourceEventChannel);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
        }

        private async Task GetSourcesToUser(developContext db)
        {
            var source = db.Source.Select(x => x).ToList();

            if (source.Count() != 0)
                try
                {
                    await Clients.Caller.SendAsync("GetSourceChain", source);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
        }

        private async Task GetSourceEventToUser(developContext db)
        {
            var sourceEvents = db.Sourceevent.Select(x => x).ToList();

            if (sourceEvents.Count() != 0)
                try
                {
                    await Clients.Caller.SendAsync("GetSourceEventsChain", sourceEvents);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
        }

        private async Task GetSourceEventChannelToUser(developContext db)
        {
            var sourceEventChannels = db.Sourceeventchannel.Select(x => x).ToList();

            if (sourceEventChannels.Count() != 0)
                try
                {
                    await Clients.Caller.SendAsync("GetSourceEventChannelsChain", sourceEventChannels);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
        }

        private async Task CheckLoggingUser(string username)
        {
            using (var db = new developContext())
            {
                var user = db.User.Select(x => x).Where(y => y.Username == username).ToList();

                if (user.Count() != 0)
                {
                    userContext.AddUser(user.Select(x => x.Username).ToString(), Context.ConnectionId);

                    connection.Add(userContext.UserName, userContext.ConnectingId);

                    var userrn = user.Select(x => x.Rn).FirstOrDefault();

                    await CheckSubscriptionsForUser(user, db);

                    await CheckLocalMessagesForConcreteUser(user, db);

                    await GetSourcesToUser(db);

                    await GetSourceEventToUser(db);

                    await GetSourceEventChannelToUser(db);

                    if (user.Select(x => x.UserAccountType).FirstOrDefault() > 1 &&
                        user.Select(x => x.UserAccountType).FirstOrDefault() < 10)
                        await Clients.Caller.SendAsync("LoginStatus", 200);

                    if (user.Select(x => x.UserAccountType).FirstOrDefault() == 999)
                        await Clients.Caller.SendAsync("LoginStatus", 207);
                }
                else
                {
                    await Clients.Caller.SendAsync("LoginStatus", 401);
                }
            }
        }
    }
}