﻿using DE.ENTITIES.Domains.Messages;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using System.Threading.Tasks;

namespace DE.WORKFLOWS.Managers.Messages
{
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class NewestMessageManager
    {
        private ConcurrentBag<MessageDto> _messageDtos;

        private ConcurrentBag<MessageChannelDto> _messageChannelDtos;

        private ConcurrentBag<MessageUserDto> _messageUserDtos;

        public NewestMessageManager()
        {
            _messageChannelDtos = new ConcurrentBag<MessageChannelDto>();
            _messageUserDtos = new ConcurrentBag<MessageUserDto>();
            _messageDtos = new ConcurrentBag<MessageDto>();
        }
        
        public ConcurrentBag<MessageDto> MessageDtos => _messageDtos;

        public ConcurrentBag<MessageChannelDto> MessageChannelDtos => _messageChannelDtos;

        public ConcurrentBag<MessageUserDto> MessageUserDtos => _messageUserDtos;

        public void Add(string data, BitArray importance, DateTime date)
        {
            var entity = new MessageDto
            {
                Rn = new Guid().GetHashCode(),
                Messagedata = data,
                Importace = importance,
                CreationDate = date
            };

            _messageDtos.Add(entity);
        }

        public void Clear()
        {
            _messageDtos.Clear();
        }
    }
}
