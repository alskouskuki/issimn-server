﻿using AutoMapper;
using DE.ENTITIES.Domains.Subs;
using DEPTHMODEL.Domains.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace INFRASTRUCTURE.MapperConfiguration.Profiles.Subs
{
    public class SubscriptionsProfile : Profile
    {
        public SubscriptionsProfile()
        {
            this.MapSubscriptions();
        }

        private void MapSubscriptions()
        {
            CreateMap<Userhchannelsubscription, UserchannelsubscriptionDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Usrn, o => o.MapFrom(x => x.Usrn))
                .ForMember(x => x.Chrn, o => o.MapFrom(x => x.Chrn))
                .ForMember(x => x.ChrnNavigation, o => o.MapFrom(x => x.ChrnNavigation))
                .ForMember(x => x.UsrnNavigation, o => o.MapFrom(x => x.UsrnNavigation));

            CreateMap<UserchannelsubscriptionDto, Userhchannelsubscription>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Usrn, o => o.MapFrom(x => x.Usrn))
                .ForMember(x => x.Chrn, o => o.MapFrom(x => x.Chrn))
                .ForMember(x => x.ChrnNavigation, o => o.MapFrom(x => x.ChrnNavigation))
                .ForMember(x => x.UsrnNavigation, o => o.MapFrom(x => x.UsrnNavigation));
        }
    }
}
