﻿using AutoMapper;
using DE.ENTITIES.Domains.Source;
using DEPTHMODEL.Domains.Sources;
using System;
using System.Collections.Generic;
using System.Text;

namespace INFRASTRUCTURE.MapperConfiguration.Profiles.Sources
{
    public class SourceProfile : Profile
    {
        public SourceProfile()
        {
            MapSource();
            MapSourceEvent();
            MapSourceEventChannel();
        }

        private void MapSource()
        {
            CreateMap<Source, SourceDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Sourcename, o => o.MapFrom(x => x.Sourcename))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.Sourceevent, o => o.MapFrom(x => x.Sourceevent))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.PrnNavigation, o => o.MapFrom(x => x.PrnNavigation))
                .ForMember(x => x.InversePrnNavigation, o => o.MapFrom(x => x.InversePrnNavigation));

            CreateMap<SourceDto, Source>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Sourcename, o => o.MapFrom(x => x.Sourcename))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.Sourceevent, o => o.MapFrom(x => x.Sourceevent))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.PrnNavigation, o => o.MapFrom(x => x.PrnNavigation))
                .ForMember(x => x.InversePrnNavigation, o => o.MapFrom(x => x.InversePrnNavigation));
        }

        private void MapSourceEvent()
        {
            CreateMap<Sourceevent, SourceeventDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Name, o => o.MapFrom(x => x.Name))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.PrnNavigation, o => o.MapFrom(x => x.PrnNavigation))
                .ForMember(x => x.Sourceeventchannel, o => o.MapFrom(x => x.Sourceeventchannel));

            CreateMap<SourceeventDto, Sourceevent>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Name, o => o.MapFrom(x => x.Name))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.PrnNavigation, o => o.MapFrom(x => x.PrnNavigation))
                .ForMember(x => x.Sourceeventchannel, o => o.MapFrom(x => x.Sourceeventchannel));
        }

        private void MapSourceEventChannel()
        {
            CreateMap<Sourceeventchannel, SourceEventChannelDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Name, o => o.MapFrom(x => x.Name))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.RnNavigation, o => o.MapFrom(x => x.RnNavigation))
                .ForMember(x => x.Messagechannel, o => o.MapFrom(x => x.Messagechannel))
                .ForMember(x => x.Userhchannelsubscription, o => o.MapFrom(x => x.Userhchannelsubscription));

            CreateMap<SourceEventChannelDto, Sourceeventchannel>()
               .PreserveReferences()
               .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
               .ForMember(x => x.Name, o => o.MapFrom(x => x.Name))
               .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
               .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
               .ForMember(x => x.RnNavigation, o => o.MapFrom(x => x.RnNavigation))
               .ForMember(x => x.Messagechannel, o => o.MapFrom(x => x.Messagechannel))
               .ForMember(x => x.Userhchannelsubscription, o => o.MapFrom(x => x.Userhchannelsubscription));
        }
    }
}
