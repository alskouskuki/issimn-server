﻿using AutoMapper;
using DE.ENTITIES.Domains.Users;
using DEPTHMODEL.Domains.Users;

namespace INFRASTRUCTURE.MapperConfiguration.Profiles.Users
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            this.MapUser();
        }

        private void MapUser()
        {
            CreateMap<User, UserDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Username, o => o.MapFrom(x => x.Username))
                .ForMember(x => x.Fullname, o => o.MapFrom(x => x.Fullname))
                .ForMember(x => x.UserRights, o => o.MapFrom(x => x.Userrights))
                .ForMember(x => x.Message, o => o.MapFrom(x => x.Message))
                .ForMember(x => x.Messageuser, o => o.MapFrom(x => x.Messageuser))
                .ForMember(x => x.Userhchannelsubscription, o => o.MapFrom(x => x.Userhchannelsubscription));

            CreateMap<UserDto, User>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Username, o => o.MapFrom(x => x.Username))
                .ForMember(x => x.Fullname, o => o.MapFrom(x => x.Fullname))
                .ForMember(x => x.Userrights, o => o.MapFrom(x => x.UserRights))
                .ForMember(x => x.Message, o => o.MapFrom(x => x.Message))
                .ForMember(x => x.Messageuser, o => o.MapFrom(x => x.Messageuser))
                .ForMember(x => x.Userhchannelsubscription, o => o.MapFrom(x => x.Userhchannelsubscription));
        }

        private void MapUserRights()
        {
            CreateMap<Userrights, UserRightsDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Userrn, o => o.MapFrom(x => x.Userrn))
                .ForMember(x => x.CanCreateSource, o => o.MapFrom(x => x.CanCreateUser))
                .ForMember(x => x.CanUpdateUser, o => o.MapFrom(x => x.CanUpdateSource))
                .ForMember(x => x.CanDeleteSource, o => o.MapFrom(x => x.CanDeleteSource))
                .ForMember(x => x.CanDeleteUser, o => o.MapFrom(x => x.CanDeleteUser))
                .ForMember(x => x.UserrnNavigation, o => o.MapFrom(x => x.UserrnNavigation));

            CreateMap<UserRightsDto, Userrights>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Userrn, o => o.MapFrom(x => x.Userrn))
                .ForMember(x => x.CanCreateSource, o => o.MapFrom(x => x.CanCreateUser))
                .ForMember(x => x.CanUpdateUser, o => o.MapFrom(x => x.CanUpdateSource))
                .ForMember(x => x.CanDeleteSource, o => o.MapFrom(x => x.CanDeleteSource))
                .ForMember(x => x.CanDeleteUser, o => o.MapFrom(x => x.CanDeleteUser))
                .ForMember(x => x.UserrnNavigation, o => o.MapFrom(x => x.UserrnNavigation));
        }
    }
}
