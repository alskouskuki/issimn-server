﻿using AutoMapper;
using DE.ENTITIES.Domains.Messages;
using DEPTHMODEL.Domains.Messages;
using DEPTHMODEL.Domains.Users;

namespace INFRASTRUCTURE.MapperConfiguration.Profiles.Messages
{
    public class MessagesProfile : Profile
    {
        public MessagesProfile()
        {
            MapMessage();
            MapMessageChannel();
            MapMessageUser();
        }

        private void MapMessage()
        {
            CreateMap<Message, MessageDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Messagedata, o => o.MapFrom(x => x.Messagedata))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.Importace, o => o.MapFrom(x => x.Importace))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.PrnNavigation, o => o.MapFrom(x => x.PrnNavigation))
                .ForMember(x => x.Messagechannel, o => o.MapFrom(x => x.Messagechannel))
                .ForMember(x => x.Messageuser, o => o.MapFrom(x => x.Messageuser));

            CreateMap<MessageDto, Message>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Messagedata, o => o.MapFrom(x => x.Messagedata))
                .ForMember(x => x.Prn, o => o.MapFrom(x => x.Prn))
                .ForMember(x => x.Importace, o => o.MapFrom(x => x.Importace))
                .ForMember(x => x.CreationDate, o => o.MapFrom(x => x.CreationDate))
                .ForMember(x => x.PrnNavigation, o => o.MapFrom(x => x.PrnNavigation))
                .ForMember(x => x.Messagechannel, o => o.MapFrom(x => x.Messagechannel))
                .ForMember(x => x.Messageuser, o => o.MapFrom(x => x.Messageuser));
        }

        private void MapMessageChannel()
        {
            CreateMap<Messagechannel, MessageChannelDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Msrn, o => o.MapFrom(x => x.Msrn))
                .ForMember(x => x.Chrn, o => o.MapFrom(x => x.Chrn))
                .ForMember(x => x.ChrnNavigation, o => o.MapFrom(x => x.ChrnNavigation))
                .ForMember(x => x.MsrnNavigation, o => o.MapFrom(x => x.MsrnNavigation));

            CreateMap<MessageChannelDto, Messagechannel>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Msrn, o => o.MapFrom(x => x.Msrn))
                .ForMember(x => x.Chrn, o => o.MapFrom(x => x.Chrn))
                .ForMember(x => x.ChrnNavigation, o => o.MapFrom(x => x.ChrnNavigation))
                .ForMember(x => x.MsrnNavigation, o => o.MapFrom(x => x.MsrnNavigation));
        }

        private void MapMessageUser()
        {
            CreateMap<Messageuser, MessageUserDto>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Msrn, o => o.MapFrom(x => x.Msrn))
                .ForMember(x => x.Usrn, o => o.MapFrom(x => x.Usrn))
                .ForMember(x => x.Read, o => o.MapFrom(x => x.Read))
                .ForMember(x => x.MsrnNavigation, o => o.MapFrom(x => x.MsrnNavigation))
                .ForMember(x => x.UsrnNavigation, o => o.MapFrom(x => x.UsrnNavigation));

            CreateMap<MessageUserDto, Messageuser>()
                .PreserveReferences()
                .ForMember(x => x.Rn, o => o.MapFrom(x => x.Rn))
                .ForMember(x => x.Msrn, o => o.MapFrom(x => x.Msrn))
                .ForMember(x => x.Usrn, o => o.MapFrom(x => x.Usrn))
                .ForMember(x => x.Read, o => o.MapFrom(x => x.Read))
                .ForMember(x => x.MsrnNavigation, o => o.MapFrom(x => x.MsrnNavigation))
                .ForMember(x => x.UsrnNavigation, o => o.MapFrom(x => x.UsrnNavigation));
        }
    }
}
