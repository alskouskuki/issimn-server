﻿using DE.DATABASELAYER.COMPONENT.Context;
using INFRASTRUCTURE.LoadManagers.DataLoaders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace INFRASTRUCTURE.LoadManagers
{
   public class L02DataLoaderManager
    {
        private readonly AllSourcePartDataLoader allSourcePartDataLoader;

        private readonly AllMessagesDataLoader allMessagesDataLoader;

        private readonly AllUserDataLoader allUserDataLoader;

        public L02DataLoaderManager()
        {
            using (developContext db = new developContext())
            {
                db.Database.OpenConnection();

                allSourcePartDataLoader = new AllSourcePartDataLoader(db);

                allMessagesDataLoader = new AllMessagesDataLoader(db);

                allUserDataLoader = new AllUserDataLoader(db);

                db.Database.CloseConnection();
            }
        }
    }
}
