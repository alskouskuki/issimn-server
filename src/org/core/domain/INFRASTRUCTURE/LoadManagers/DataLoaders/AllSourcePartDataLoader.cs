﻿using DE.DATABASELAYER.COMPONENT.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace INFRASTRUCTURE.LoadManagers.DataLoaders
{
    public class AllSourcePartDataLoader
    {
        public AllSourcePartDataLoader(developContext db)
        {
            this.SourceDataLoader(db);
            this.SourceEventDataLoader(db);
            this.SourceEventChannelDataLoader(db);
        }

        private void SourceDataLoader(developContext db)
        {
            db.Source.Load();
        }

        private void SourceEventDataLoader(developContext db)
        {
            db.Sourceevent.Load();
        }

        private void SourceEventChannelDataLoader(developContext db)
        {
            db.Sourceeventchannel.Load();
        }
    }
}
