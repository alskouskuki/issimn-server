﻿using DE.DATABASELAYER.COMPONENT.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INFRASTRUCTURE.LoadManagers.DataLoaders
{
    public class AllMessagesDataLoader
    {
        public AllMessagesDataLoader(developContext db)
        {
            this.MessageDataLoader(db);
        }

        private void MessageDataLoader(developContext db)
        {
            db.Message.Load();
        }
    }
}
