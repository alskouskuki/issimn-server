﻿using DE.DATABASELAYER.COMPONENT.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INFRASTRUCTURE.LoadManagers.DataLoaders
{
    public class AllUserDataLoader
    {
        public AllUserDataLoader(developContext db)
        {
            this.UserDataLoader(db);
        }

        private void UserDataLoader(developContext db)
        {
            db.User.Load();
        }
    }
}
