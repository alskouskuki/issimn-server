﻿using AutoMapper;
using INFRASTRUCTURE.Configurators.mappers;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Messages;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Sources;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Subs;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Users;
using System.ComponentModel.Composition;

namespace INFRASTRUCTURE.LoadManagers
{
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class L01SystemLoader
    {
        private DevAutoMapperConfiguration devAutoMapperConfiguration;

        public L01SystemLoader()
        {
            devAutoMapperConfiguration = new DevAutoMapperConfiguration();

            devAutoMapperConfiguration.Configure();
        }
    }
}
