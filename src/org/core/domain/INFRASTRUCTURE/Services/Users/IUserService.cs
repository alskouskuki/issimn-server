﻿using DE.ENTITIES.Domains.Users;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DE.ENTITIES.Domains.Source;
using DE.ENTITIES.Domains.Subs;
using DEPTHMODEL.Mappings.Subs;

namespace INFRASTRUCTURE.Services.Users
{
    public interface IOpenUserService : ICustomServiceBase
    {
        IList<UserDto> GetUserByName(string username);

        IList<UserRightsDto> GetRightsOfUser(IList<UserDto> users);

        void CreateSubscription(UserDto user, SourceEventChannelDto sourceEventChannel);

        void UpdateSubscription(UserchannelsubscriptionDto userChannelSubscription);

        void DeleteSubscription(UserchannelsubscriptionDto userChannelSubscription);
    }

    public interface IAdminUserService : ICustomServiceBase

    {
        void CreateUser(string username, string fullname, int accountType);

        void AddPermissionsToUser(UserRightsDto userRights);

        void UpdateUser(UserDto user);

        void UpdateRightsToUser(UserRightsDto userRights);

        void DeleteUser(UserDto user);

        void DeleteUserRights(UserRightsDto userRights);
    }
}
