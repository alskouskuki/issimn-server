﻿using DE.ENTITIES.Domains.Source;
using System;
using System.Collections.Generic;
using System.Text;

namespace INFRASTRUCTURE.Services.Source
{
    public interface ISourceManipulationService : ICustomServiceBase
    {
        void CreateSource(string Name, SourceDto source);

        void CreateSourceEvent(string Name, SourceDto source);

        void UpdateSource(SourceDto source);

        void UpdateSourceEvent(SourceeventDto sourceEvent);

        void DeleteSource(SourceDto source);

        void DeleteSourceEvent(SourceeventDto sourceEvent);
    }
}
