﻿using AutoMapper;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Messages;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Sources;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Subs;
using INFRASTRUCTURE.MapperConfiguration.Profiles.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;

namespace INFRASTRUCTURE.Configurators.mappers
{
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class DevAutoMapperConfiguration
    {
        public void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<SourceProfile>();
                x.AddProfile<UserProfile>();
                x.AddProfile<SubscriptionsProfile>();
                x.AddProfile<MessagesProfile>();
            });

            Mapper.Configuration.AssertConfigurationIsValid();
        }
    }
}
