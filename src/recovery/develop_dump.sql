--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: depth; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA depth;


ALTER SCHEMA depth OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: MESSAGE; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."MESSAGE" (
    "RN" integer NOT NULL,
    "PRN" integer NOT NULL,
    "IMPORTACE" bit(1),
    "CREATION_DATE" date,
    "MESSAGEDATA" character varying(350) NOT NULL
);


ALTER TABLE depth."MESSAGE" OWNER TO postgres;

--
-- Name: MESSAGECHANNEL; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."MESSAGECHANNEL" (
    "RN" integer NOT NULL,
    "MSRN" integer NOT NULL,
    "CHRN" integer NOT NULL
);


ALTER TABLE depth."MESSAGECHANNEL" OWNER TO postgres;

--
-- Name: MESSAGEUSER; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."MESSAGEUSER" (
    "RN" integer NOT NULL,
    "MSRN" integer NOT NULL,
    "USRN" integer NOT NULL,
    "READ" bit(1)
);


ALTER TABLE depth."MESSAGEUSER" OWNER TO postgres;

--
-- Name: SOURCE; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."SOURCE" (
    "RN" integer NOT NULL,
    "SOURCENAME" character varying(150) NOT NULL,
    "CREATION_DATE" date NOT NULL,
    "PRN" integer
);


ALTER TABLE depth."SOURCE" OWNER TO postgres;

--
-- Name: SOURCEEVENT; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."SOURCEEVENT" (
    "RN" integer NOT NULL,
    "NAME" character varying(150) NOT NULL,
    "PRN" integer NOT NULL,
    "CREATION_DATE" date NOT NULL
);


ALTER TABLE depth."SOURCEEVENT" OWNER TO postgres;

--
-- Name: SOURCEEVENTCHANNEL; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."SOURCEEVENTCHANNEL" (
    "RN" integer NOT NULL,
    "NAME" character varying(150) NOT NULL,
    "PRN" integer NOT NULL,
    "CREATION_DATE" date NOT NULL
);


ALTER TABLE depth."SOURCEEVENTCHANNEL" OWNER TO postgres;

--
-- Name: USER; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."USER" (
    "RN" integer NOT NULL,
    "USERNAME" character varying(40) NOT NULL,
    "FULLNAME" character varying(150) NOT NULL
);


ALTER TABLE depth."USER" OWNER TO postgres;

--
-- Name: USERHCHANNELSUBSCRIPTION; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."USERHCHANNELSUBSCRIPTION" (
    "RN" integer NOT NULL,
    "USRN" integer NOT NULL,
    "CHRN" integer NOT NULL
);


ALTER TABLE depth."USERHCHANNELSUBSCRIPTION" OWNER TO postgres;

--
-- Name: USERRIGHTS; Type: TABLE; Schema: depth; Owner: postgres
--

CREATE TABLE depth."USERRIGHTS" (
    "RN" integer NOT NULL,
    "USERRN" integer NOT NULL,
    "CanCreateSource" bit(1) NOT NULL,
    "CanCreateUser" bit(1) NOT NULL,
    "CanUpdateSource" bit(1) NOT NULL,
    "CanUpdateUser" bit(1) NOT NULL,
    "CanDeleteSource" bit(1) NOT NULL,
    "CanDeleteUser" bit(1) NOT NULL
);


ALTER TABLE depth."USERRIGHTS" OWNER TO postgres;

--
-- Data for Name: MESSAGE; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."MESSAGE" ("RN", "PRN", "IMPORTACE", "CREATION_DATE", "MESSAGEDATA") FROM stdin;
1	1	1	\N	Hello there
2	1	1	\N	Hello there too
3	2	1	\N	Hmm
\.


--
-- Data for Name: MESSAGECHANNEL; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."MESSAGECHANNEL" ("RN", "MSRN", "CHRN") FROM stdin;
\.


--
-- Data for Name: MESSAGEUSER; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."MESSAGEUSER" ("RN", "MSRN", "USRN", "READ") FROM stdin;
1	1	1	0
2	2	2	0
3	3	1	0
\.


--
-- Data for Name: SOURCE; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."SOURCE" ("RN", "SOURCENAME", "CREATION_DATE", "PRN") FROM stdin;
\.


--
-- Data for Name: SOURCEEVENT; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."SOURCEEVENT" ("RN", "NAME", "PRN", "CREATION_DATE") FROM stdin;
\.


--
-- Data for Name: SOURCEEVENTCHANNEL; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."SOURCEEVENTCHANNEL" ("RN", "NAME", "PRN", "CREATION_DATE") FROM stdin;
\.


--
-- Data for Name: USER; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."USER" ("RN", "USERNAME", "FULLNAME") FROM stdin;
1	Admin	Admin Adminovich
2	Default	User 0
\.


--
-- Data for Name: USERHCHANNELSUBSCRIPTION; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."USERHCHANNELSUBSCRIPTION" ("RN", "USRN", "CHRN") FROM stdin;
\.


--
-- Data for Name: USERRIGHTS; Type: TABLE DATA; Schema: depth; Owner: postgres
--

COPY depth."USERRIGHTS" ("RN", "USERRN", "CanCreateSource", "CanCreateUser", "CanUpdateSource", "CanUpdateUser", "CanDeleteSource", "CanDeleteUser") FROM stdin;
\.


--
-- Name: MESSAGECHANNEL MESSAGECHANNEL_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGECHANNEL"
    ADD CONSTRAINT "MESSAGECHANNEL_pkey" PRIMARY KEY ("RN");


--
-- Name: MESSAGEUSER MESSAGEUSER_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGEUSER"
    ADD CONSTRAINT "MESSAGEUSER_pkey" PRIMARY KEY ("RN");


--
-- Name: MESSAGE MESSAGE_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGE"
    ADD CONSTRAINT "MESSAGE_pkey" PRIMARY KEY ("RN");


--
-- Name: SOURCEEVENTCHANNEL SOURCEEVENTCHANNEL_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."SOURCEEVENTCHANNEL"
    ADD CONSTRAINT "SOURCEEVENTCHANNEL_pkey" PRIMARY KEY ("RN");


--
-- Name: SOURCEEVENT SOURCEEVENT_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."SOURCEEVENT"
    ADD CONSTRAINT "SOURCEEVENT_pkey" PRIMARY KEY ("RN");


--
-- Name: SOURCE SOURCE_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."SOURCE"
    ADD CONSTRAINT "SOURCE_pkey" PRIMARY KEY ("RN");


--
-- Name: USERHCHANNELSUBSCRIPTION USERHCHANNELSUBSCRIPTION_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."USERHCHANNELSUBSCRIPTION"
    ADD CONSTRAINT "USERHCHANNELSUBSCRIPTION_pkey" PRIMARY KEY ("RN");


--
-- Name: USERRIGHTS USERRIGHTS_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."USERRIGHTS"
    ADD CONSTRAINT "USERRIGHTS_pkey" PRIMARY KEY ("RN");


--
-- Name: USER User_pkey; Type: CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."USER"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY ("RN");


--
-- Name: SOURCEEVENTCHANNEL CHANNEL_FK_PRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."SOURCEEVENTCHANNEL"
    ADD CONSTRAINT "CHANNEL_FK_PRN" FOREIGN KEY ("RN") REFERENCES depth."SOURCEEVENT"("RN");


--
-- Name: MESSAGE MESS_FK_PRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGE"
    ADD CONSTRAINT "MESS_FK_PRN" FOREIGN KEY ("PRN") REFERENCES depth."USER"("RN");


--
-- Name: MESSAGECHANNEL MSGCHNL_FK_MSRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGECHANNEL"
    ADD CONSTRAINT "MSGCHNL_FK_MSRN" FOREIGN KEY ("MSRN") REFERENCES depth."MESSAGE"("RN");


--
-- Name: MESSAGECHANNEL MSGCNHL_FK_CHRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGECHANNEL"
    ADD CONSTRAINT "MSGCNHL_FK_CHRN" FOREIGN KEY ("CHRN") REFERENCES depth."SOURCEEVENTCHANNEL"("RN");


--
-- Name: MESSAGEUSER MSGUSER_FK_MSRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGEUSER"
    ADD CONSTRAINT "MSGUSER_FK_MSRN" FOREIGN KEY ("MSRN") REFERENCES depth."MESSAGE"("RN");


--
-- Name: MESSAGEUSER MSGUSER_FK_USRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."MESSAGEUSER"
    ADD CONSTRAINT "MSGUSER_FK_USRN" FOREIGN KEY ("USRN") REFERENCES depth."USER"("RN");


--
-- Name: SOURCEEVENT SOURCEEVENT_FK_PRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."SOURCEEVENT"
    ADD CONSTRAINT "SOURCEEVENT_FK_PRN" FOREIGN KEY ("PRN") REFERENCES depth."SOURCE"("RN");


--
-- Name: SOURCE SOURCE_FK_PRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."SOURCE"
    ADD CONSTRAINT "SOURCE_FK_PRN" FOREIGN KEY ("PRN") REFERENCES depth."SOURCE"("RN");


--
-- Name: USERHCHANNELSUBSCRIPTION SUB_FK_CHRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."USERHCHANNELSUBSCRIPTION"
    ADD CONSTRAINT "SUB_FK_CHRN" FOREIGN KEY ("CHRN") REFERENCES depth."SOURCEEVENTCHANNEL"("RN");


--
-- Name: USERHCHANNELSUBSCRIPTION SUB_FK_USRN; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."USERHCHANNELSUBSCRIPTION"
    ADD CONSTRAINT "SUB_FK_USRN" FOREIGN KEY ("USRN") REFERENCES depth."USER"("RN");


--
-- Name: USERRIGHTS USERRIGHT_PRN_USER; Type: FK CONSTRAINT; Schema: depth; Owner: postgres
--

ALTER TABLE ONLY depth."USERRIGHTS"
    ADD CONSTRAINT "USERRIGHT_PRN_USER" FOREIGN KEY ("USERRN") REFERENCES depth."USER"("RN");


--
-- PostgreSQL database dump complete
--

